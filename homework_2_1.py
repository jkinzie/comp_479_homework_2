from machine_learning import *
import sys

#############################################################################
print(50 * '=')
print('Section: Training a perceptron model on the Iris dataset')
print(50 * '-')

#df = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None)
#df1.to_csv('iris.csv', header=None)


df = pd.read_csv('click.csv',header=None, dtype=np.int64)

pd.set_option('display.width', 200)
pd.set_option('display.max_columns', None)

print(df)



#############################################################################
print(50 * '=')
print('Plotting the click data')
print(50 * '-')

first_click_down_times = (df[0]).astype(np.int64)
deadspace_times = (df[1]).astype(np.int64)
second_click_down_times = (df[2]).astype(np.int64)
Successful_click = (df[3]).astype(np.int64)

FT = first_click_down_times[:15]
FF = first_click_down_times[15:]

DT = deadspace_times[:15]
DF = deadspace_times[15:]

ST = second_click_down_times[:15]
SF = second_click_down_times[15:]

X = df.iloc[:, 1:3]
Y = df.iloc[:, -1]

print(X)
print(Y)

plt.rcParams["figure.figsize"] = [16,9]
# plt.yscale("log")

plt.ylabel('Time')

plt.tight_layout()

x = ('First Click Success', 'Second Click Success', 'Deadspace Success', 'First Click Failure', 'Second Click Failure', 'Deadspace Failure')
# x = ['category']

for element in FT:
    plt.scatter(x[0], element, color='blue', marker='.')

for element in ST:
    plt.scatter(x[1], element, color='blue', marker='.')

for element in DT:
    plt.scatter(x[2], element, color='blue', marker='.')

for element in FF:
    plt.scatter(x[3], element, color='red', marker='.')

for element in SF:
    plt.scatter(x[4], element, color='red', marker='.')

for element in DF:
    plt.scatter(x[5], element, color='red', marker='.')

plt.show()


#############################################################################
print(50 * '=')
print('Training the perceptron model')
print(50 * '-')

ppn = Perceptron(eta=0.1, n_iter=10)

ppn.fit(X, Y)

plt.plot(range(1, len(ppn.errors_) + 1), ppn.errors_, marker='o')
plt.xlabel('Epochs')
plt.ylabel('Number of misclassifications')

# plt.tight_layout()
# plt.savefig('./perceptron_1.png', dpi=300)
plt.show()

#############################################################################
print(50 * '=')
print('A function for plotting decision regions')
print(50 * '-')

# sys.exit()
plot_decision_regions(X, Y, classifier=ppn)
plt.xlabel('sepal length [cm]')
plt.ylabel('petal length [cm]')
plt.legend(loc='upper left')

# plt.tight_layout()
# plt.savefig('./perceptron_2.png', dpi=300)
plt.show()

sys.exit()

#############################################################################
print(50 * '=')
print('Implementing an adaptive linear neuron in Python')
print(50 * '-')






fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(8, 4))

ada1 = AdalineGD(n_iter=10, eta=0.01).fit(X, y)
ax[0].plot(range(1, len(ada1.cost_) + 1), np.log10(ada1.cost_), marker='o')
ax[0].set_xlabel('Epochs')
ax[0].set_ylabel('log(Sum-squared-error)')
ax[0].set_title('Adaline - Learning rate 0.01')

ada2 = AdalineGD(n_iter=10, eta=0.0001).fit(X, y)
ax[1].plot(range(1, len(ada2.cost_) + 1), ada2.cost_, marker='o')
ax[1].set_xlabel('Epochs')
ax[1].set_ylabel('Sum-squared-error')
ax[1].set_title('Adaline - Learning rate 0.0001')

# plt.tight_layout()
# plt.savefig('./adaline_1.png', dpi=300)
plt.show()


print('standardize features')
X_std = np.copy(X)
X_std[:, 0] = (X[:, 0] - X[:, 0].mean()) / X[:, 0].std()
X_std[:, 1] = (X[:, 1] - X[:, 1].mean()) / X[:, 1].std()

ada = AdalineGD(n_iter=15, eta=0.01)
ada.fit(X_std, y)

plot_decision_regions(X_std, y, classifier=ada)
plt.title('Adaline - Gradient Descent')
plt.xlabel('sepal length [standardized]')
plt.ylabel('petal length [standardized]')
plt.legend(loc='upper left')
# plt.tight_layout()
# plt.savefig('./adaline_2.png', dpi=300)
plt.show()

plt.plot(range(1, len(ada.cost_) + 1), ada.cost_, marker='o')
plt.xlabel('Epochs')
plt.ylabel('Sum-squared-error')

# plt.tight_layout()
# plt.savefig('./adaline_3.png', dpi=300)
plt.show()


#############################################################################
print(50 * '=')
print('Large scale machine learning and stochastic gradient descent')
print(50 * '-')

ada = AdalineSGD(n_iter=15, eta=0.01, random_state=1)
ada.fit(X_std, y)

plot_decision_regions(X_std, y, classifier=ada)
plt.title('Adaline - Stochastic Gradient Descent')
plt.xlabel('sepal length [standardized]')
plt.ylabel('petal length [standardized]')
plt.legend(loc='upper left')

# plt.tight_layout()
# plt.savefig('./adaline_4.png', dpi=300)
plt.show()

plt.plot(range(1, len(ada.cost_) + 1), ada.cost_, marker='o')
plt.xlabel('Epochs')
plt.ylabel('Average Cost')

# plt.tight_layout()
# plt.savefig('./adaline_5.png', dpi=300)
plt.show()

ada = ada.partial_fit(X_std[0, :], y[0])